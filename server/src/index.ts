require('dotenv').config();
import 'reflect-metadata'
import express from 'express'
import { createConnection } from 'typeorm';

const main = async() => {
    await createConnection({
        type:'postgres',
        host: '34.87.63.28',
        database:process.env.DB_DATABASE_DEV,
        username: process.env.DB_USERNAME_DEV,
        password:process.env.DB_PASSWORD_DEV,
        logging:true,
        synchronize:true
    })

    const app = express()

    app.listen(4000, () => {
        console.log('Server started on port 4000');
        
    })

    
}
main().catch(error => {
    console.log(error);
    
})